# Tower Defense

### A tower defense game made in Processing

We made this game for fun, and we work on it in our free time. Feel free to clone the repo and play it for yourself!

To make it work, please install [Processing](https://processing.org/download) and include the core.jar as a library in this project.  
Core.jar can be found in the processing folder under /core/library.  
Openjdk 18 recommended, it might say it has errors, but it works fine.

If you have any suggestions or find any game-breaking bugs please create an issue for them, so they can be resolved/added later :) Have fun!
