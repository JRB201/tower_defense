package Improvements;

import Models.Improvement;
import Models.Tower_Defense;
import Models.Weapon;
import Weapons.*;
import processing.core.PConstants;
import processing.core.PVector;

public class Brewery extends Improvement {

    public Brewery(Tower_Defense tower_defense, PVector position) {
        super(tower_defense, position);
        image = tower_defense.utils.brewery;
        name = "Brewery";
        description = "Place these next to ballistas to give them poison arrows";
        cost = 3500;
        checkNearbyBallistas();
    }

    public void checkNearbyBallistas() {
        PVector above = new PVector(position.x, position.y - 50);
        PVector below = new PVector(position.x, position.y + 50);
        PVector left = new PVector(position.x - 50, position.y);
        PVector right = new PVector(position.x + 50, position.y);
        for (Weapon w : tower_defense.weapons) {
            if (w instanceof Ballista temp) {
                if (w.position.equals(above) || w.position.equals(below) || w.position.equals(left) || w.position.equals(right)) {
                    temp.nextToBrewery = true;
                }
            }
        }

    }

    @Override
    public void draw() {
        tower_defense.imageMode(PConstants.CENTER);
        tower_defense.image(image, position.x, position.y);
        if (tower_defense.frameCount % 60 == 0) {
            checkNearbyBallistas();
        }
    }

    @Override
    public Brewery dupe() {
        return new Brewery(tower_defense, position.copy());
    }
}
