package Improvements;

import Models.Improvement;
import Models.Projectile;
import Models.Tower_Defense;
import processing.core.PConstants;
import processing.core.PVector;

public class Wall extends Improvement {
    int uses;
    int currentUses;

    public Wall(Tower_Defense tower_defense, PVector position) {
        super(tower_defense, position);
        image = tower_defense.utils.wall;
        uses = 25;
        currentUses = 0;
        cost = 2500;
        name = "Wall";
        description = "Walls block things";
    }

    @Override
    public void draw() {
        if (uses > currentUses) {
            tower_defense.imageMode(PConstants.CENTER);

            tower_defense.image(image, position.x, position.y);
        } else {
            tower_defense.claimedPoints.remove(position);
            tower_defense.improvements.remove(this);
        }
        onHit();
    }

    public void onHit() {
        for (int i = 0; i < tower_defense.spawnedEnemies.size(); i++) {
            for (int j = 0; j < tower_defense.spawnedEnemies.get(i).projectiles.size(); j++) {
                if (collidesWithProjectile(tower_defense.spawnedEnemies.get(i).projectiles.get(j))) {
                    tower_defense.spawnedEnemies.get(i).projectiles.remove(j);
                    currentUses++;
                    j--; //reduce loop index by 1 to account for shift in list caused by earlier List.remove() call
                }
            }
        }

    }

    @Override
    public Wall dupe() {
        return new Wall(tower_defense, new PVector(position.x, position.y));
    }

    protected boolean collidesWithProjectile(Projectile p) {
        return position.x - image.width / 2f < p.position.x - p.projectile.width / 2f + p.projectile.width
                && (position.x - image.width / 2f) + image.width > p.position.x - p.projectile.width / 2f
                && position.y - image.height / 2f < (p.position.y - p.projectile.height / 2f) + p.projectile.height
                && (position.y - image.height / 2f) + image.height > p.position.y - p.projectile.height / 2f;
    }

}
