package Enemies;

import Models.Enemy;
import Models.Tower_Defense;
import processing.core.PApplet;
import processing.core.PVector;

public class Tank extends Enemy {

    public Tank(Tower_Defense tower_defense) {
        super(tower_defense);
        this.hp = 500;
        this.maxHp = hp;
        this.image = tower_defense.utils.tank;
        image.resize(45, 50);
        this.speed = 1;
        this.reward = 150;
        this.difficulty = 10;
        this.startingLevel = 7;
    }

    @Override
    public void onDeath() {
        super.onDeath();
        boolean horizontal;
        int distanceApart = 15;
        PVector next = tower_defense.path.coors.get(current);
        horizontal = PApplet.abs(next.y - position.y) == 0;
        for (int i = 1; i < 4; i++) {
            Enemy e = new StandardGoblin(tower_defense);
            e.position = this.position.copy();
            if(horizontal) {
                e.position.x = e.position.x - distanceApart + (i * distanceApart);
            }else{
                e.position.y = e.position.y - distanceApart + (i * distanceApart);
            }
            e.current = this.current;
            tower_defense.spawnedEnemies.add(e);
        }
        for (int i = 1; i < 3; i++) {
            Enemy e = new SpeedyGoblin(tower_defense);
            if(horizontal) {
                e.position.x = e.position.x - distanceApart + (i * distanceApart);
            }else{
                e.position.y = e.position.y - distanceApart + (i * distanceApart);
            }

            e.position = this.position.copy();
            e.current = this.current;
            tower_defense.spawnedEnemies.add(e);
        }
    }
}
