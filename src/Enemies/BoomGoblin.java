package Enemies;

import Models.Drawable;
import Models.Enemy;
import Models.StatusEffect;
import Models.Tower_Defense;
import StatusEffects.OnFire;
import processing.core.PVector;

public class BoomGoblin extends Enemy {
    private final int explosionDamage;

    public BoomGoblin(Tower_Defense tower_defense) {
        super(tower_defense);
        this.hp = 250;
        this.maxHp = hp;
        this.image = tower_defense.utils.biemGoblin;
        image.resize(35, 50);
        this.speed = 1;
        this.reward = 100;
        this.difficulty = 5;
        this.startingLevel = 10;
        this.explosionDamage = 100;

    }

    @Override
    public void onDeath(){
        super.onDeath();
        tower_defense.drawables.add(new Drawable(tower_defense, position,tower_defense.utils.explosion, 10));
        for (int i = 0; i < tower_defense.spawnedEnemies.size(); i++) {
            Enemy enemy = tower_defense.spawnedEnemies.get(i);
            if (PVector.dist(position, enemy.position) < 100) {
                enemy.hp -= explosionDamage * (1-(PVector.dist(position, enemy.position) / 100));
                enemy.addStatusEffect(new OnFire(tower_defense,enemy,1));
                if (enemy.hp <= 0) {
                    tower_defense.spawnedEnemies.remove(enemy);
                }

            }
        }
    }
    @Override
    public void draw(){
        super.draw();
        if (statusEffects.size() > 0){
            for (StatusEffect s: statusEffects) {
                if(s instanceof OnFire){
                    this.onDeath();
                    return;
                }
            }
        }
    }
}
