package Enemies;

import Models.Enemy;
import Models.Tower_Defense;

public class SpeedyGoblin extends Enemy {

    public SpeedyGoblin(Tower_Defense tower_defense) {
        super(tower_defense);
        this.hp = 75;
        this.maxHp = hp;
        this.image = tower_defense.utils.speedyGoblin;
        image.resize(25, 30);
        this.speed = 3;
        this.reward = 100;
        this.difficulty = 3;
        this.startingLevel = 5;
    }
}
