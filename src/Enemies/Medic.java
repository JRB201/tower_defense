package Enemies;

import Models.Drawable;
import Models.Enemy;
import Models.StatusEffect;
import Models.Tower_Defense;
import StatusEffects.OnFire;
import processing.core.PVector;

public class Medic extends Enemy {
    private final int damage;
    private int cooldown;
    private final int maxCooldown;

    public Medic(Tower_Defense tower_defense) {
        super(tower_defense);
        this.hp = 125;
        this.maxHp = hp;
        this.image = tower_defense.utils.medic;
        image.resize(25, 35);
        this.speed = 1;
        this.reward = 100;
        this.difficulty = 15;
        this.startingLevel = 15;
        this.damage = -50;
        this.cooldown = 100;
        this.maxCooldown = cooldown;

    }
    @Override
    public void draw(){
        super.draw();
        if(cooldown <= 0){
            tower_defense.drawables.add(new Drawable(tower_defense, position,tower_defense.utils.healing, 10));
            for (int i = 0; i < tower_defense.spawnedEnemies.size(); i++) {
                Enemy enemy = tower_defense.spawnedEnemies.get(i);
                if (PVector.dist(position, enemy.position) < 250) {
                    enemy.hp = (int) Tower_Defense.constrain(enemy.hp - damage * (1-(PVector.dist(position, enemy.position) / 250)), 0, enemy.maxHp);
                }
            }
            cooldown = maxCooldown;
        }
        cooldown--;

    }
}
