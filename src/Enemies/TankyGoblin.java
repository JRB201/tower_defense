package Enemies;

import Models.Enemy;
import Models.Tower_Defense;

public class TankyGoblin extends Enemy {

    public TankyGoblin(Tower_Defense tower_defense) {
        super(tower_defense);
        this.hp = 200;
        this.maxHp = hp;
        this.image = tower_defense.utils.tankyGoblin;
        image.resize(25, 30);
        this.speed = 0.5f;
        this.reward = 100;
        this.difficulty = 2;
        this.startingLevel = 3;
    }
}
