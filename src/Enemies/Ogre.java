package Enemies;

import Models.Enemy;
import Models.Projectile;
import Models.Tower_Defense;
import processing.core.PVector;


public class Ogre extends Enemy {

    private final int range;
    private int cooldown;
    private final int maxCooldown;


    public Ogre(Tower_Defense tower_defense) {
        super(tower_defense);
        this.hp = 500;
        this.maxHp = hp;
        this.image = tower_defense.utils.ogre;
        image.resize(50, 75);
        this.speed = 1;
        this.reward = 100;
        this.difficulty = 25;
        this.startingLevel = 20;
        this.damage = 50;
        this.cooldown = 250;
        this.maxCooldown = cooldown;
        this.range = 1000;
        this.projectileImg = tower_defense.utils.ogreBall;
        this.projectileSpeed = 2;
    }

    @Override
    public void draw() {
        super.draw();
        drawProjectiles();
        if (cooldown <= 0 && PVector.dist(tower_defense.tower.position, this.position) < range) {
            angle = tower_defense.angleBetween(tower_defense.tower.position, position);
            cooldown = maxCooldown;
            shoot();
        }
        cooldown--;
    }

    private void drawProjectiles() {
        for (Projectile p : projectiles) {
            p.draw();
        }
    }

    protected void shoot() {
        projectiles.add(new Projectile(tower_defense, this));
    }
}
