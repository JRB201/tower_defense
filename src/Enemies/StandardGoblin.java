package Enemies;

import Models.Enemy;
import Models.Tower_Defense;

public class StandardGoblin extends Enemy {

    public StandardGoblin(Tower_Defense tower_defense) {
        super(tower_defense);
        this.hp = 100;
        this.maxHp = hp;
        this.image = tower_defense.utils.standardGoblin;
        image.resize(25, 30);
        this.speed = 1;
        this.reward = 50;
        this.difficulty = 1;
        this.startingLevel = 1;
    }
}
