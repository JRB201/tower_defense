package Models;

import processing.core.PImage;
import processing.core.PVector;

public class Improvement {
    public Tower_Defense tower_defense;
    public PVector position;
    public PImage image;
    public int cost;
    public String name;
    public String description;

    public Improvement(Tower_Defense tower_defense, PVector position){
        this.tower_defense = tower_defense;
        this.position = position;
    }
    public Improvement dupe() {
        return null;
    }

    public void draw(){
    }
}
