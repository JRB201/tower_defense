package Models;

import processing.core.PApplet;
import processing.core.PVector;

import java.util.ArrayList;

public class Tower_Defense extends PApplet {
    public Utils utils;

    public Tower tower;
    public ArrayList<PVector> claimedPoints;
    public ArrayList<Enemy> spawnedEnemies;
    ArrayList<Enemy> enemies;
    public ArrayList<Weapon> weapons;
    public ArrayList<Consumable> consumables;
    public ArrayList<Improvement> improvements;
    Shop shop;
    ArrayList<Wave> waves;
    public ArrayList<Drawable> drawables;
    public Path path;
    Weapon currentSelectedWeapon;
    Consumable currentSelectedConsumable;
    Improvement currentSelectedImprovement;
    Wave currentWave;
    int currentWaveNumber;
    int gracePeriod;
    int spawnTimer;
    public int coins;
    //    int background = color(48, 119, 43);
    int red = color(250, 50, 50);
    int yellow = color(250, 250, 50);
    int green = color(50, 250, 50);
    int brown = color(183, 117, 58);
    int middlebrown = color(135, 86, 51);
    int darkbrown = color(104, 66, 39);
    int grey = color(200);
    int startingDifficulty = 10;
    boolean DEBUG = false;

    public void settings() {
        size(1000, 1000);
        utils = new Utils(this);

        utils.setup();

        setClaimedPoints();
        currentWaveNumber = 1;
        waves.add(new Wave(this, 20));
        currentWave = waves.get(0);
        gracePeriod = 600;
        shop.positionShopItems();
        spawnTimer = (int) frameRate;
        coins = 1750;
        if (DEBUG) {
            coins = 1000000;
        }
    }

    public void draw() {
        if (tower.hp > 0) {
            background(utils.background);
            circle(mouseX, mouseY, 20);
            utils.draw();
            if (DEBUG) {
                line(width / 2f, 0, width / 2f, height);
                drawClaimedPoints();
                textSize(20);
                text(frameRate, 20, 20);
            }
            utils.waves();
            utils.currentSelectedWeapon();
            utils.currentSelectedConsumable();
            utils.currentSelectedImprovement();
            utils.showDescription();
        } else {
            gameOver();
        }
        fill(255);

    }

    public boolean positionOnTower(PVector v) {
        return v.x == tower.position.x && v.y == tower.position.y || v.x == tower.position.x && v.y == tower.position.y - 50;
    }

//    protected void shop() {
//        fill(brown);
//        rectMode(CORNER);
//        rect(0, height - 100, width, 100);
//        fill(0);
//        for (ShopItem i : shop) {
//            i.draw();
//        }
//        imageMode(CENTER);
//        image(utils.upButton, width / 2f, height - 100);
//        imageMode(CORNER);
//        image(utils.coin, 20, height - 95);
//        textSize(25);
//        textAlign(CORNER);
//        text(coins, 25 + utils.coin.width, height - 95 + utils.coin.height / 1.5f);
//        image(utils.wave, 20, height - 50);
//        text(currentWaveNumber, 25 + utils.wave.width, height - 50 + utils.wave.height / 1.5f);
//    }

    public void mousePressed() {
        if (mouseButton == LEFT) {
            utils.clickShop();
            utils.clickWeapons();
        }
    }

    public void gameOver() {
        background(200);
        fill(0);
        textAlign(CENTER);
        textSize(60);
        text("daar ga je", width / 2f, height / 3f);
        textSize(40);
        text("volgende keer beter i guess lol", width / 2f, height / 2f);
    }

    public void drawEnemies() {
        for (int i = spawnedEnemies.size() - 1; i > 0; i--) {
            spawnedEnemies.get(i).draw();
        }
    }

    public void drawWeapons() {
        for (Weapon w : weapons) {
            w.draw();
        }
    }
    public void drawWeaponMenu(){
        for (Weapon w : weapons) {
            w.drawMenu();
        }
    }

    public void drawConsumables() {
        for (int i = 0; i < consumables.size(); i++) {
            Consumable c = consumables.get(i);
            c.draw();
        }
    }

    public void drawDrawables() {
        for (int i = 0; i < drawables.size(); i++) {
            drawables.get(i).draw();
            if (drawables.get(i).expireTimer == 0) {
                drawables.remove(drawables.get(i));
            }
        }
    }

    public void drawImprovements() {
        for (int i = 0; i < improvements.size(); i++) {
            improvements.get(i).draw();
        }
    }

    protected void checkCollision() {
        for (int j = 0; j < spawnedEnemies.size(); j++) {
            Enemy e = spawnedEnemies.get(j);
            if (e.current == path.coors.size()) {
                tower.hp = constrain(tower.hp - e.hp, 0, tower.maxHp);
                spawnedEnemies.remove(e);
                continue;
            }
            for (Weapon w : weapons) {
                for (int i = 0; i < w.projectiles.size(); i++) {
                    Projectile p = w.projectiles.get(i);
                    if (collidesWithEnemy(p, e)) {
                        w.onHit(e, p);
                    }
                }
            }
        }
        for (Enemy e : spawnedEnemies) {
            for (int j = 0; j < e.projectiles.size(); j++) {
                Projectile p = e.projectiles.get(j);
                if (collidesWithTower(p, tower)) {
                    tower.hp = constrain(tower.hp - e.damage, 0, tower.maxHp);
                    e.projectiles.remove(p);
                }
            }
        }
    }

    protected boolean collidesWithEnemy(Projectile p, Enemy e) {
        return p.position.x - p.projectile.width / 2f < e.position.x - e.image.width / 2f + e.image.width
                && (p.position.x - p.projectile.width / 2f) + p.projectile.width > e.position.x - e.image.width / 2f
                && p.position.y - p.projectile.height / 2f < (e.position.y - e.image.height / 2f) + e.image.height
                && (p.position.y - p.projectile.height / 2f) + p.projectile.height > e.position.y - e.image.height / 2f;
    }

    protected boolean collidesWithTower(Projectile p, Tower t) {
        return p.position.x - p.projectile.width / 2f < t.position.x - t.image.width / 2f + t.image.width
                && (p.position.x - p.projectile.width / 2f) + p.projectile.width > t.position.x - t.image.width / 2f
                && p.position.y - p.projectile.height / 2f < (t.position.y - t.image.height / 2f) + t.image.height
                && (p.position.y - p.projectile.height / 2f) + p.projectile.height > t.position.y - t.image.height / 2f;
    }

    public static void main(String[] args) {
        PApplet.main("Models.Tower_Defense");
    }

    public void setClaimedPoints() {
        PVector prev = null;
        for (PVector p : path.coors) {
            if (prev != null) {
                claimPointsBetween(prev, p, claimedPoints);
            }
            claimedPoints.add(p);
            prev = p;
        }
        assert prev != null;
        claimedPoints.add(new PVector(prev.x, prev.y - 50));
    }

    public void drawClaimedPoints() {
        for (PVector p : claimedPoints) {
            fill(grey);
            circle(p.x, p.y, 10);
        }
    }

    public PVector gridSnapping() {
        PVector p;
        if (mouseX > 25 && mouseX < width - 25 && mouseY > 25 && mouseY < height - 125) {
            int x = mouseX % 50;
            int y = mouseY % 50;

            PVector temp = new PVector();
            temp.x = x >= 25 ? mouseX + (50 - x) : mouseX - x;
            temp.y = y >= 25 ? mouseY + (50 - y) : mouseY - y;

            p = temp;
        } else {
            p = new PVector(mouseX, mouseY);
        }
        return p;
    }

    public boolean onClaimedPoint(PVector p) {
        for (PVector v : claimedPoints) {
            if (p.equals(v)) {
                return true;
            }
        }
        return false;
    }

    public void claimPointsBetween(PVector prev, PVector p, ArrayList<PVector> list) {
        for (int i = 0; i < PVector.dist(prev, p) / 50; i++) {
            if (prev.x != p.x) {
                if (prev.x < p.x) {
                    list.add(new PVector(prev.x + (i * 50), prev.y));
                } else {
                    list.add(new PVector(prev.x - (i * 50), prev.y));
                }
            } else if (prev.y != p.y) {
                if (prev.y < p.y) {
                    list.add(new PVector(prev.x, prev.y + (i * 50)));
                } else {
                    list.add(new PVector(prev.x, prev.y - (i * 50)));
                }
            }
        }
    }


    public float angleBetween(PVector destination, PVector origin) {
        return PVector.sub(destination, origin).heading();
    }


    public void closeMenus(Weapon w) {
        for (Weapon weapon : weapons) {
            if (w != weapon) {
                weapon.menuOpen = false;
            }
        }
    }

}
