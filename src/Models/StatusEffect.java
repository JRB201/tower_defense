package Models;

import java.util.ArrayList;

public class StatusEffect {
    protected final Tower_Defense tower_defense;
    protected int duration;
    protected int timeLeft;
    public Enemy enemy;
    public int filter;
    public ArrayList<Particle> particles;

    public StatusEffect(Tower_Defense tower_defense, Enemy enemy, int duration) {
        this.tower_defense = tower_defense;
        this.enemy = enemy;
        this.duration = (int) (duration * tower_defense.frameRate);
        this.timeLeft = this.duration;
        this.filter = tower_defense.color(255, 100);
        this.particles = new ArrayList<>();
    }

    public void onApply() {
        System.out.println("biem! status effect man");
    }

    public void whileApplied(){
        System.out.println("biem! status effect is actief man");
        this.timeLeft--;
    }

    public void onExpire() {
        System.out.println("biem! status effect weg man");
    }

    public boolean equals(StatusEffect e) {
        return e != null;
    }

    public void resetTimer() {
        this.timeLeft = duration;
    }
}
