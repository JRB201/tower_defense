package Models;

import Enemies.*;

import java.util.ArrayList;

class Wave {
    private final Tower_Defense tower_defense;
    int difficulty;
    int currentDifficulty;

    ArrayList<Enemy> enemiesToSpawn;

    public Wave(Tower_Defense tower_defense, int difficulty) {
        this.tower_defense = tower_defense;
        this.difficulty = difficulty;
        this.currentDifficulty = 0;
        this.enemiesToSpawn = new ArrayList<>();
        generateEnemies();
    }

    public void generateEnemies() {
        while (currentDifficulty < difficulty) {
            int random = (int) tower_defense.random(0, tower_defense.enemies.size());
            if (tower_defense.enemies.get(random).difficulty <= Math.abs(difficulty - currentDifficulty) && tower_defense.enemies.get(random).startingLevel <= tower_defense.currentWaveNumber) {
                switch (random) {
                    case 0 -> enemiesToSpawn.add(new StandardGoblin(tower_defense));
                    case 1 -> enemiesToSpawn.add(new SpeedyGoblin(tower_defense));
                    case 2 -> enemiesToSpawn.add(new TankyGoblin(tower_defense));
                    case 3 -> enemiesToSpawn.add(new Tank(tower_defense));
                    case 4 -> enemiesToSpawn.add(new BoomGoblin(tower_defense));
                    case 5 -> enemiesToSpawn.add(new Medic(tower_defense));
                    case 6 -> enemiesToSpawn.add(new Ogre(tower_defense));
                }
                currentDifficulty += tower_defense.enemies.get(random).difficulty;
            }
        }
    }

    public void spawnEnemies() {
        if (tower_defense.spawnTimer == 0 && !enemiesToSpawn.isEmpty()) {
            tower_defense.spawnedEnemies.add(enemiesToSpawn.get(0));
            enemiesToSpawn.remove(0);
            tower_defense.spawnTimer = (int) tower_defense.frameRate - tower_defense.currentWaveNumber;
        } else {
            tower_defense.spawnTimer--;
        }
    }
}
