package Models;

import Consumables.Bomb;
import Consumables.CarpetBomb;
import Consumables.Medkit;
import Consumables.Spikes;
import Enemies.*;
import Improvements.Brewery;
import Improvements.Wall;
import Weapons.*;
import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

import java.util.ArrayList;


public class Utils {
    protected final Tower_Defense tower_defense;

    public PImage tower;
    public PImage coin;
    public PImage standardGoblin;
    public PImage speedyGoblin;
    public PImage tankyGoblin;
    public PImage tank;
    public PImage base;
    public PImage laser_base;
    public PImage ballista;
    public PImage cannon;
    public PImage laser;
    public PImage bolt;
    public PImage cannonball;
    public PImage laserbolt;
    public PImage explosion;
    public PImage fire;
    public PImage fireBall;
    public PImage fireBase;
    public PImage ice;
    public PImage iceBase;
    public PImage snowBall;
    public PImage background;
    public PImage path_1h;
    public PImage path_2h;
    public PImage path_3h;
    public PImage path_1v;
    public PImage path_2v;
    public PImage path_3v;
    public PImage path_corner;
    public PImage path_corner_90;
    public PImage path_corner_180;
    public PImage path_corner_270;
    public PImage biemGoblin;
    public PImage healing;
    public PImage medic;
    public PImage ogre;
    public PImage emptybase;
    public PImage bomb;
    public PImage spikes;
    public PImage wave;
    public PImage wall;
    public PImage ogreBall;
    public PImage medkit;
    public PImage upButton;
    public PImage downButton;
    public PImage brewery;

    public Utils(Tower_Defense tower_defense) {
        this.tower_defense = tower_defense;
        loadImages();
    }

    private void loadImages() {
        tower = tower_defense.loadImage("imgs/tower.png");
        coin = tower_defense.loadImage("imgs/coin.png");
        standardGoblin = tower_defense.loadImage("imgs/goblin.png");
        speedyGoblin = tower_defense.loadImage("imgs/speedygoblin.png");
        tankyGoblin = tower_defense.loadImage("imgs/tankygoblin.png");
        tank = tower_defense.loadImage("imgs/peerd.png");
        base = tower_defense.loadImage("imgs/base.png");
        laser_base = tower_defense.loadImage("imgs/laser_base.png");
        ballista = tower_defense.loadImage("imgs/ballista.png");
        cannon = tower_defense.loadImage("imgs/cannon.png");
        laser = tower_defense.loadImage("imgs/laser.png");
        bolt = tower_defense.loadImage("imgs/bolt.png");
        cannonball = tower_defense.loadImage("imgs/cannonball.png");
        laserbolt = tower_defense.loadImage("imgs/laserbolt.png");
        explosion = tower_defense.loadImage("imgs/explosion.png");
        fire = tower_defense.loadImage("imgs/fire.png");
        fireBall = tower_defense.loadImage("imgs/fire_ball.png");
        fireBase = tower_defense.loadImage("imgs/fire_base.png");
        iceBase = tower_defense.loadImage("imgs/ice_base.png");
        ice = tower_defense.loadImage("imgs/snow_cannon.png");
        snowBall = tower_defense.loadImage("imgs/snowball.png");
        background = tower_defense.loadImage("imgs/background.png");
        path_1h = tower_defense.loadImage("imgs/path_1_horizontal.png");
        path_2h = tower_defense.loadImage("imgs/path_2_horizontal.png");
        path_3h = tower_defense.loadImage("imgs/path_3_horizontal.png");
        path_1v = tower_defense.loadImage("imgs/path_1.png");
        path_2v = tower_defense.loadImage("imgs/path_2.png");
        path_3v = tower_defense.loadImage("imgs/path_3.png");
        path_corner = tower_defense.loadImage("imgs/path_corner.png");
        path_corner_90 = tower_defense.loadImage("imgs/path_corner_90.png");
        path_corner_180 = tower_defense.loadImage("imgs/path_corner_180.png");
        path_corner_270 = tower_defense.loadImage("imgs/path_corner_270.png");
        biemGoblin = tower_defense.loadImage("imgs/biemgoblim.png");
        healing = tower_defense.loadImage("imgs/healing.png");
        medic = tower_defense.loadImage("imgs/medic.png");
        ogre = tower_defense.loadImage("imgs/ogre.png");
        emptybase = tower_defense.loadImage("imgs/emptybase.png");
        bomb = tower_defense.loadImage("imgs/bomb.png");
        spikes = tower_defense.loadImage("imgs/spikes.png");
        wave = tower_defense.loadImage("imgs/wave.png");
        wall = tower_defense.loadImage("imgs/wall.png");
        ogreBall = tower_defense.loadImage("imgs/ogreball.png");
        medkit = tower_defense.loadImage("imgs/medkit.png");
        upButton = tower_defense.loadImage("imgs/up_button.png");
        downButton = tower_defense.loadImage("imgs/down_button.png");
        brewery = tower_defense.loadImage("imgs/brewery.png");
    }

    public void setup() {
        this.initializeArraylists();
        tower_defense.path = new Path(tower_defense);
        this.addEnemies();
        tower_defense.tower = new Tower(tower_defense, 1000, tower_defense.path.coors.get(tower_defense.path.coors.size() - 1).x, tower_defense.path.coors.get(tower_defense.path.coors.size() - 1).y, tower);
        this.addShop();
    }

    public void addEnemies() {
        tower_defense.enemies.add(new StandardGoblin(tower_defense));
        tower_defense.enemies.add(new SpeedyGoblin(tower_defense));
        tower_defense.enemies.add(new TankyGoblin(tower_defense));
        tower_defense.enemies.add(new Tank(tower_defense));
        tower_defense.enemies.add(new BoomGoblin(tower_defense));
        tower_defense.enemies.add(new Medic(tower_defense));
        tower_defense.enemies.add(new Ogre(tower_defense));
    }

    public void addShop() {
        tower_defense.shop = new Shop(tower_defense);
        tower_defense.shop.weapons.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Ballista(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y))); // 3000 frames = 2500 dmg
        tower_defense.shop.weapons.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Cannon(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y))); // 3000 frames = 2400 dmg
        tower_defense.shop.weapons.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Laser(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y))); //3000 frames = 3600 dmg
        tower_defense.shop.weapons.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Fire(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y)));
        tower_defense.shop.weapons.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Ice(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y)));
        tower_defense.shop.consumables.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Bomb(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y)));
        tower_defense.shop.consumables.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Spikes(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y)));
        tower_defense.shop.consumables.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new CarpetBomb(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y)));
        tower_defense.shop.consumables.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Medkit(tower_defense, tower_defense.tower.position.x, tower_defense.tower.position.y)));

        tower_defense.shop.improvements.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Wall(tower_defense, new PVector(tower_defense.tower.position.x, tower_defense.tower.position.y))));
        tower_defense.shop.improvements.add(new ShopItem(tower_defense, 0, tower_defense.height - 50, new Brewery(tower_defense, new PVector(tower_defense.tower.position.x, tower_defense.tower.position.y))));
    }

    public void initializeArraylists() {
        tower_defense.spawnedEnemies = new ArrayList<>();
        tower_defense.enemies = new ArrayList<>();
        tower_defense.weapons = new ArrayList<>();
        tower_defense.consumables = new ArrayList<>();
        tower_defense.waves = new ArrayList<>();
        tower_defense.claimedPoints = new ArrayList<>();
        tower_defense.drawables = new ArrayList<>();
        tower_defense.improvements = new ArrayList<>();
    }

    public void draw() {

        tower_defense.path.draw();
        tower_defense.tower.draw();
        tower_defense.drawEnemies();
        tower_defense.drawConsumables();
        tower_defense.drawDrawables();
        tower_defense.checkCollision();
        tower_defense.drawWeapons();
        tower_defense.drawImprovements();
        tower_defense.shop.draw();
        tower_defense.drawWeaponMenu();

    }

    public void currentSelectedWeapon() {
        if (tower_defense.currentSelectedWeapon != null) {
            PVector closest = tower_defense.gridSnapping();
            tower_defense.ellipseMode(PConstants.CENTER);
            if (tower_defense.onClaimedPoint(closest)) {
                tower_defense.fill(tower_defense.red, 128);
            } else {
                tower_defense.fill(tower_defense.yellow, 128);
            }

            tower_defense.noStroke();
            tower_defense.circle(closest.x, closest.y, tower_defense.currentSelectedWeapon.shootingRadius);
            tower_defense.imageMode(PConstants.CENTER);
            tower_defense.image(tower_defense.currentSelectedWeapon.base, closest.x, closest.y);
            tower_defense.image(tower_defense.currentSelectedWeapon.weapon, closest.x, closest.y);
        }
    }

    public void currentSelectedConsumable() {
        if (tower_defense.currentSelectedConsumable != null) {
            PVector closest = tower_defense.gridSnapping();
            tower_defense.currentSelectedConsumable.position = closest;
            tower_defense.ellipseMode(PConstants.CENTER);
            if (tower_defense.currentSelectedConsumable.allowedPositions()) {
                tower_defense.fill(tower_defense.yellow, 128);
            } else {
                tower_defense.fill(tower_defense.red, 128);
            }

            tower_defense.noStroke();
            tower_defense.circle(closest.x, closest.y, tower_defense.currentSelectedConsumable.range * 2);
            tower_defense.imageMode(PConstants.CENTER);
            tower_defense.image(emptybase, closest.x, closest.y);
            tower_defense.image(tower_defense.currentSelectedConsumable.image, closest.x, closest.y);
        }
    }

    public void currentSelectedImprovement() {
        if (tower_defense.currentSelectedImprovement != null) {
            PVector closest = tower_defense.gridSnapping();
            tower_defense.ellipseMode(PConstants.CENTER);
            if (tower_defense.onClaimedPoint(closest)) {
                tower_defense.fill(tower_defense.red, 128);
            } else {
                tower_defense.fill(tower_defense.yellow, 128);
            }

            tower_defense.noStroke();
            tower_defense.circle(closest.x, closest.y, 100);
            tower_defense.imageMode(PConstants.CENTER);
            tower_defense.image(tower_defense.currentSelectedImprovement.image, closest.x, closest.y);
        }
    }

    public void showDescription() {
        tower_defense.shop.showDescriptions();
    }

    public void waves() {
        if (tower_defense.gracePeriod == 0) {
            tower_defense.currentWave.spawnEnemies();
            if (tower_defense.currentWave.enemiesToSpawn.isEmpty()) {
                tower_defense.waves.add(new Wave(tower_defense, (int) (tower_defense.startingDifficulty * (Math.pow(tower_defense.waves.size(), 1.2)))));
                tower_defense.currentWaveNumber++;
                tower_defense.currentWave = tower_defense.waves.get(tower_defense.currentWaveNumber - 1);
                tower_defense.gracePeriod = 1200;
            }
        } else {
            tower_defense.gracePeriod--;
        }
    }

    public void clickShop() {
        ShopItem item = null;
        if (shopButton()) {
            return;
        }
        if (shopFavorites()) {
            return;
        }
        if (tower_defense.shop.isOpen) {
            item = shopOthers(item);
        }
        shopPlaceItems(item);
    }

    public void clickWeapons() {
        for (int i = 0; i < tower_defense.weapons.size(); i++) {
            tower_defense.weapons.get(i).onClickListener();
            if (tower_defense.weapons.get(i).menuOpen) {
                tower_defense.closeMenus(tower_defense.weapons.get(i));
                for (MenuItem m : tower_defense.weapons.get(i).menu.priorities) {
                    if (m.onClick()) {
                        tower_defense.weapons.get(i).priority = tower_defense.weapons.get(i).menu.priorities.indexOf(m);
                    }
                }
                for (MenuItem m : tower_defense.weapons.get(i).menu.upgrades) {
                    if (m.onClick()) {
                        tower_defense.weapons.get(i).upgrade(m);
                    }
                }
                for (MenuItem m : tower_defense.weapons.get(i).menu.sell) {
                    if (m.onClick()) {
                        tower_defense.coins += tower_defense.weapons.get(i).cost / 2;
                        tower_defense.claimedPoints.remove(tower_defense.weapons.get(i).position);
                        tower_defense.weapons.remove(tower_defense.weapons.get(i));
                    }
                }
            }
        }
    }

    public boolean shopButton() {
        if (tower_defense.mouseX > tower_defense.width / 2f - upButton.width / 2f
                && tower_defense.mouseX < tower_defense.width / 2f + upButton.width / 2f
                && tower_defense.mouseY > (tower_defense.height - 100 - upButton.height / 2f)
                && tower_defense.mouseY < (tower_defense.height - 100 + upButton.height / 2f)) {
            tower_defense.shop.isOpen = !tower_defense.shop.isOpen;
            return true;
        }
        return false;
    }

    public boolean shopFavorites() {
        for (int i = 0; i < tower_defense.shop.favorites.size(); i++) {
            if (tower_defense.shop.favorites.get(i).mouseOverShopItem()) {
                if (tower_defense.currentSelectedWeapon != null) {
                    for (ShopItem shopItem : tower_defense.shop.weapons) {
                        if (shopItem.weapon.name.equals(tower_defense.currentSelectedWeapon.name)) {
                            PVector position = tower_defense.shop.favorites.get(i).position;
                            tower_defense.shop.favorites.set(i, new ShopItem(shopItem));
                            tower_defense.shop.favorites.get(i).position = position;
                            tower_defense.currentSelectedWeapon = null;
                            return true;
                        }
                    }
                }
                if (tower_defense.currentSelectedConsumable != null) {
                    for (ShopItem shopItem : tower_defense.shop.consumables) {
                        if (shopItem.consumable.name.equals(tower_defense.currentSelectedConsumable.name)) {
                            PVector position = tower_defense.shop.favorites.get(i).position;
                            tower_defense.shop.favorites.set(i, new ShopItem(shopItem));
                            tower_defense.shop.favorites.get(i).position = position;
                            tower_defense.currentSelectedConsumable = null;
                            return true;
                        }
                    }
                }
                if (tower_defense.currentSelectedImprovement != null) {
                    for (ShopItem shopItem : tower_defense.shop.improvements) {
                        if (shopItem.improvement.name.equals(tower_defense.currentSelectedImprovement.name)) {
                            PVector position = tower_defense.shop.favorites.get(i).position;
                            tower_defense.shop.favorites.set(i, new ShopItem(shopItem));
                            tower_defense.shop.favorites.get(i).position = position;
                            tower_defense.currentSelectedImprovement = null;
                            return true;
                        }
                    }
                }
                if (tower_defense.shop.favorites.get(i).weapon != null) {
                    tower_defense.currentSelectedWeapon = tower_defense.shop.favorites.get(i).weapon.dupe();
                }
                if (tower_defense.shop.favorites.get(i).consumable != null) {
                    tower_defense.currentSelectedConsumable = tower_defense.shop.favorites.get(i).consumable.dupe();
                }
                if (tower_defense.shop.favorites.get(i).improvement != null) {
                    tower_defense.currentSelectedImprovement = tower_defense.shop.favorites.get(i).improvement.dupe();
                }
                return true;
            }
        }
        return false;
    }

    public ShopItem shopOthers(ShopItem item) {
        for (ShopItem s : tower_defense.shop.weapons) {
            if (s.weapon != null && s.mouseOverShopItem()) {
                item = s;
                tower_defense.currentSelectedWeapon = s.weapon;
                tower_defense.closeMenus(null);
            }
        }
        for (ShopItem s : tower_defense.shop.consumables) {
            if (s.consumable != null && s.mouseOverShopItem()) {
                item = s;
                tower_defense.currentSelectedConsumable = s.consumable;
                tower_defense.closeMenus(null);
            }
        }
        for (ShopItem s : tower_defense.shop.improvements) {
            if (s.improvement != null && s.mouseOverShopItem()) {
                item = s;
                tower_defense.currentSelectedImprovement = s.improvement;
                tower_defense.closeMenus(null);
            }
        }
        if (item != null && item.price > tower_defense.coins) {
            tower_defense.currentSelectedWeapon = null;
            tower_defense.currentSelectedConsumable = null;
            tower_defense.currentSelectedImprovement = null;
        } else if (item != null) {
            tower_defense.shop.isOpen = false;
        }
        return item;
    }

    public void shopPlaceItems(ShopItem item) {
        if (item != null && item.mouseOverShopItem()) {
            if (item.weapon != null) {
                tower_defense.currentSelectedWeapon = item.weapon;
                tower_defense.closeMenus(null);
            }
            if (item.consumable != null) {
                tower_defense.currentSelectedConsumable = item.consumable;
                tower_defense.closeMenus(null);
            }
            if (item.improvement != null) {
                tower_defense.currentSelectedImprovement = item.improvement;
                tower_defense.closeMenus(null);
            }
        } else if (tower_defense.currentSelectedWeapon != null) {
            System.out.println(tower_defense.currentSelectedWeapon);
            PVector closest = tower_defense.gridSnapping();
            Weapon w = tower_defense.currentSelectedWeapon.dupe();
            if (tower_defense.mouseY > tower_defense.height - 125) {
                tower_defense.currentSelectedWeapon = null;
                System.out.println("hallo");
                return;

            }
            if (w != null) {
                w.position = closest;
                if (!tower_defense.onClaimedPoint(w.position)) {
                    tower_defense.coins -= w.cost;
                    tower_defense.weapons.add(w);
                    tower_defense.claimedPoints.add(w.position.copy());
                    tower_defense.currentSelectedWeapon = null;
                    tower_defense.closeMenus(w);
                }
            }
        } else if (tower_defense.currentSelectedConsumable != null) {
            PVector closest = tower_defense.gridSnapping();
            Consumable c = tower_defense.currentSelectedConsumable.dupe();
            if (tower_defense.mouseY > tower_defense.height - 125) {
                tower_defense.currentSelectedConsumable = null;
                return;
            }
            if (c != null) {
                c.position = closest;
                if (c.allowedPositions()) {
                    tower_defense.coins -= c.cost;
                    tower_defense.consumables.add(c);
                    tower_defense.currentSelectedConsumable = null;
                }
            }
        } else if (tower_defense.currentSelectedImprovement != null) {
            PVector closest = tower_defense.gridSnapping();
            Improvement i = tower_defense.currentSelectedImprovement.dupe();
            if (tower_defense.mouseY > tower_defense.height - 125) {
                tower_defense.currentSelectedImprovement = null;
                return;
            }
            if (i != null) {
                i.position = closest;
                if (!tower_defense.onClaimedPoint(i.position)) {
                    tower_defense.claimedPoints.add(i.position.copy());
                    tower_defense.improvements.add(i);
                    tower_defense.coins -= i.cost;
                    tower_defense.currentSelectedImprovement = null;
                }
            }
        }
    }
}
