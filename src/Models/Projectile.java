package Models;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public class Projectile {
    private final Tower_Defense tower_defense;
    public PVector position;
    public PImage projectile;
    int speed;
    float angle;

    public Projectile(Tower_Defense tower_defense, Weapon weapon) {
        this.tower_defense = tower_defense;
        this.position = new PVector(weapon.position.x, weapon.position.y);
        this.speed = weapon.projectileSpeed;
        this.projectile = weapon.projectileImg;
        projectile.resize(8, 8);
        this.angle = weapon.angle;
    }
    public Projectile(Tower_Defense tower_defense, Enemy enemy) {
        this.tower_defense = tower_defense;
        this.position = new PVector(enemy.position.x, enemy.position.y);
        this.speed = enemy.projectileSpeed;
        this.projectile = enemy.projectileImg;
        projectile.resize(8, 8);
        this.angle = enemy.angle;
    }

    public void draw() {
        updatePosition();
        tower_defense.pushMatrix();
        tower_defense.translate(position.x, position.y);
        tower_defense.rotate(angle + Tower_Defense.radians(90));
        tower_defense.translate(-position.x, -position.y);
        tower_defense.image(projectile, position.x, position.y);
        tower_defense.popMatrix();
    }

    protected void updatePosition() {
        position.x += PApplet.cos(angle) * speed;
        position.y += PApplet.sin(angle) * speed;
    }
}
