package Models;

import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

public class Drawable {
    Tower_Defense tower_defense;
    PVector position;
    PImage image;
    int expireTimer;

    public Drawable(Tower_Defense tower_defense, PVector position, PImage image, int expireTimer) {
        this.tower_defense = tower_defense;
        this.position = position;
        this.image = image;
        this.expireTimer = expireTimer;
    }

    public void draw() {
        tower_defense.imageMode(PConstants.CENTER);
        tower_defense.image(image, position.x, position.y);
        expireTimer--;
    }
}
