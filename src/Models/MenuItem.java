package Models;

import processing.core.PConstants;
import processing.core.PVector;

public class MenuItem {
    private final Tower_Defense tower_defense;
    PVector position;
    String text;
    int cost;
    int amount;

    public MenuItem(Tower_Defense tower_defense, String text, int amount) {
        position = new PVector();
        this.tower_defense = tower_defense;
        this.text = text;
        this.amount = amount;
    }

    public MenuItem(Tower_Defense tower_defense, String text, int amount, int cost) {
        this(tower_defense, text, amount);
        this.cost = cost;
    }

    public void draw(float x, float y, boolean selected) {
        position.x = x;
        position.y = y;
        tower_defense.rectMode(PConstants.CENTER);
        if (selected) {
            tower_defense.fill(tower_defense.middlebrown);
        } else {
            tower_defense.fill(tower_defense.darkbrown);
        }
        tower_defense.rect(position.x, position.y, 200f / amount - 5, 30);
        tower_defense.textAlign(PConstants.CENTER);
        tower_defense.fill(255);
        tower_defense.textSize(8);
        if (cost != 0) {
            tower_defense.text(text + "\n" + cost, position.x, position.y);
        } else {
            tower_defense.text(text, position.x, position.y);
        }
    }

    public boolean onClick() {
        return tower_defense.mouseX < position.x + (200f / amount - 5) / 2
                && tower_defense.mouseX > position.x - (200f / amount - 5) / 2
                && tower_defense.mouseY < position.y + 15
                && tower_defense.mouseY > position.y - 15;
    }
}
