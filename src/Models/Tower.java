package Models;

import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

public class Tower {
    private final Tower_Defense tower_defense;
    public int hp;
    public int maxHp;
    PImage image;
    public PVector position;

    public void draw() {
        tower_defense.imageMode(PConstants.CENTER);
        tower_defense.image(image, position.x, position.y - tower_defense.utils.tower.height / 4f);
        tower_defense.imageMode(PConstants.CORNER);
        if (hp != maxHp) {
            healthbar();
        }
    }

    Tower(Tower_Defense tower_defense, int hp, float x, float y, PImage towerImg) {
        this.tower_defense = tower_defense;
        this.hp = hp;
        this.maxHp = hp;
        position = new PVector(x, y);
        this.image = towerImg;
        image.resize(60, 100);
    }

    public void healthbar() {
        tower_defense.fill(0);
        int hpcolor = 0;
        tower_defense.rectMode(PConstants.CORNER);
        tower_defense.rect(position.x - image.width / 2f, position.y - image.height / 1.5f, image.width, 10);
        if ((float) hp / maxHp > 0.7) {
            hpcolor = tower_defense.green;
        } else if ((float) hp / maxHp < 0.7 && (float) hp / maxHp > 0.4) {
            hpcolor = tower_defense.yellow;
        } else if ((float) hp / maxHp < 0.4) {
            hpcolor = tower_defense.red;
        }
        tower_defense.fill(hpcolor);
        tower_defense.rect(position.x - image.width / 2f, position.y - image.height / 1.5f, image.width * ((float) hp / maxHp), 10);
    }
}
