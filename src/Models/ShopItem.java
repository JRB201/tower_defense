package Models;

import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

import java.util.Objects;

class ShopItem {
    private final Tower_Defense tower_defense;
    Weapon weapon;
    Consumable consumable;
    Improvement improvement;
    PImage base;
    PImage weaponImg;
    PVector position;
    int price;

    public ShopItem(Tower_Defense tower_defense, float x, float y, Weapon weapon) {
        this.tower_defense = tower_defense;
        this.position = new PVector(x, y);
        this.base = weapon.base;
        this.weapon = weapon;
        this.weaponImg = weapon.weapon;
        this.price = weapon.cost;
    }

    public ShopItem(Tower_Defense tower_defense, float x, float y, Consumable consumable) {
        this.tower_defense = tower_defense;
        this.position = new PVector(x, y);
        this.base = tower_defense.utils.emptybase;
        this.weaponImg = consumable.image;
        this.consumable = consumable;
        this.price = consumable.cost;
    }

    public ShopItem(Tower_Defense tower_defense, float x, float y, Improvement improvement) {
        this.tower_defense = tower_defense;
        this.position = new PVector(x, y);
        this.base = tower_defense.utils.emptybase;
        this.weaponImg = improvement.image;
        this.improvement = improvement;
        this.price = improvement.cost;
    }

    public ShopItem(Tower_Defense tower_defense, float x, float y) {
        this.tower_defense = tower_defense;
        this.position = new PVector(x, y);
        this.base = tower_defense.utils.emptybase;
        this.weaponImg = tower_defense.utils.emptybase;
        this.price = 0;
    }

    public ShopItem(ShopItem s) {
        this.tower_defense = s.tower_defense;
        this.position = s.position;
        this.weaponImg = s.weaponImg;
        this.base = s.base;
        this.price = s.price;
        this.weapon = s.weapon;
        this.consumable = s.consumable;
        this.improvement = s.improvement;
    }

    public void draw() {
        tower_defense.rectMode(PConstants.CENTER);
        tower_defense.fill(0);
        tower_defense.rect(position.x, position.y, 40, 40);
        tower_defense.fill(tower_defense.brown);
        tower_defense.rect(position.x, position.y, 34, 34);
        tower_defense.imageMode(PConstants.CENTER);
        tower_defense.image(base, position.x, position.y);
        tower_defense.image(weaponImg, position.x, position.y);
        tower_defense.rectMode(PConstants.CORNER);
        tower_defense.imageMode(PConstants.CORNER);
        tower_defense.fill(0);
        tower_defense.textAlign(PConstants.CENTER);
        tower_defense.textSize(12);
        if (price > 0) {
            tower_defense.text(price, position.x, position.y + base.height * 1.2f);
        }
    }

    public boolean mouseOverShopItem() {
        return tower_defense.mouseX > position.x - base.width / 2f && tower_defense.mouseX < position.x + base.width / 2f && tower_defense.mouseY > position.y - base.height / 2f && tower_defense.mouseY < position.y + base.height / 2f;
    }


    public void displayDescription() {
        String name = "";
        String description = "";
        if (weapon != null) {
            name = weapon.name;
            description = weapon.description;
        }
        if (consumable != null) {
            name = consumable.name;
            description = consumable.description;
        }
        if (improvement != null) {
            name = improvement.name;
            description = improvement.description;
        }
        if (!Objects.equals(description, "") && !Objects.equals(name, "")) {
            tower_defense.rectMode(PConstants.CENTER);
            tower_defense.fill(0);
            tower_defense.rect(position.x, position.y - 50, tower_defense.textWidth(description) / 2 + 10, 30);
            tower_defense.fill(tower_defense.brown);
            tower_defense.rect(position.x, position.y - 50, tower_defense.textWidth(description) / 2 + 8, 28);
            tower_defense.fill(0);
            tower_defense.textAlign(PConstants.CENTER);
            tower_defense.textSize(12);
            tower_defense.text(name, position.x, position.y - 50);
            tower_defense.textSize(10);
            tower_defense.text(description, position.x, position.y - 40);
        }
    }
}
