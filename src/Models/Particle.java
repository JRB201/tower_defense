package Models;

import processing.core.PVector;

public class Particle {
    public Tower_Defense tower_defense;

    public StatusEffect statusEffect;
    public PVector location;
    public String shape;
    public int duration;

    public Particle(Tower_Defense tower_defense,StatusEffect statusEffect, PVector location, String shape, int duration){
        this.tower_defense = tower_defense;
        this.statusEffect = statusEffect;
        this.location = new PVector(location.x, location.y);
        this.shape = shape;
        this.duration = duration;

    }

    public void draw(){
        System.out.println("ik teken een particle");
        duration--;
    }


}
