package Models;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

import java.util.ArrayList;

public class Enemy {
    protected final Tower_Defense tower_defense;
    public int hp;
    public int maxHp;
    public float speed;
    public PImage image;
    public PVector position;
    public int current;
    public int reward;
    public int difficulty;
    protected ArrayList<StatusEffect> statusEffects;
    public ArrayList<Projectile> projectiles;

    public int damage;

    public int startingLevel;
    public int projectileSpeed;

    public PImage projectileImg;
    public float angle;

    public Enemy(Tower_Defense tower_defense) {
        this.tower_defense = tower_defense;
        position = new PVector(tower_defense.path.coors.get(0).x, tower_defense.path.coors.get(0).y);
        this.current = 0;
        statusEffects = new ArrayList<>();
        projectiles = new ArrayList<>();
        damage = 0;
    }

    public void draw() {
        if (statusEffects.size() > 0) {
            for (int i = 0; i < statusEffects.size(); i++) {
                StatusEffect s = statusEffects.get(i);
                s.whileApplied();
                tower_defense.tint(statusEffectsColor());
                if (s.timeLeft <= 0) {
                    s.onExpire();
                    statusEffects.remove(s);
                }
            }
        }
        if (current < tower_defense.path.coors.size()) {
            updatePosition();
        }
        if (hp != maxHp) {
            healthbar();
        }
        if (hp <= 0) {
            onDeath();
        }
        tower_defense.noTint();

    }

    public void addStatusEffect(StatusEffect e) {
        for (StatusEffect effect : statusEffects) {
            if (effect.equals(e)) {
                effect.resetTimer();
                return;
            }
        }
        statusEffects.add(e);
        e.onApply();
    }

    public void onDeath() {
        tower_defense.coins += reward;
        tower_defense.spawnedEnemies.remove(this);
    }

    protected void updatePosition() {
        if (PVector.dist(position, tower_defense.path.coors.get(current)) <= speed) {
            position.x = tower_defense.path.coors.get(current).x;
            position.y = tower_defense.path.coors.get(current).y;
            current++;
            tower_defense.imageMode(PConstants.CENTER);
            tower_defense.image(image, position.x, position.y - image.height / 3f);
            return;
        }

        tower_defense.imageMode(PConstants.CENTER);
        tower_defense.pushMatrix();
        if (position.x > tower_defense.path.coors.get(current).x) {
            position.x -= speed;
            tower_defense.scale(-1, 1);
            tower_defense.image(image, -position.x, position.y - image.height / 3f);
        }
        if (position.x < tower_defense.path.coors.get(current).x) {
            position.x += speed;
            tower_defense.image(image, position.x, position.y - image.height / 3f);
        }
        if (position.y > tower_defense.path.coors.get(current).y) {
            position.y -= speed;
            tower_defense.image(image, position.x, position.y - image.height / 3f);
        }
        if (position.y < tower_defense.path.coors.get(current).y) {
            position.y += speed;
            tower_defense.image(image, position.x, position.y - image.height / 3f);
        }
        tower_defense.popMatrix();
        tower_defense.imageMode(PConstants.CORNER);
    }

    protected void healthbar() {
        tower_defense.fill(0);
        int hpcolor = 0;
        tower_defense.rectMode(PConstants.CORNER);
        tower_defense.rect(position.x - image.width / 2f, position.y - image.height - 7.5f, image.width, 7.5f);
        if ((float) hp / maxHp > 0.7) {
            hpcolor = tower_defense.green;
        } else if ((float) hp / maxHp < 0.7 && (float) hp / maxHp > 0.4) {
            hpcolor = tower_defense.yellow;
        } else if ((float) hp / maxHp < 0.4) {
            hpcolor = tower_defense.red;
        }
        tower_defense.fill(hpcolor);
        tower_defense.rect(position.x - image.width / 2f, position.y - image.height - 7.5f, image.width * ((float) hp / maxHp), 7.5f);
    }

    public int statusEffectsColor() {
        int color = 0;
        for (StatusEffect s : statusEffects) {
            color = PApplet.blendColor(color, s.filter, PConstants.ADD);
        }
        return color;
    }
}
