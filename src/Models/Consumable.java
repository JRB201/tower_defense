package Models;

import processing.core.PImage;
import processing.core.PVector;

public class Consumable {
    protected final Tower_Defense tower_defense;
    public PVector position;

    public PImage image;
    public int timer;
    public int currentTime;
    public int damage;
    public int range;
    public int cost;
    public String name;
    public String description;


    protected Consumable(Tower_Defense tower_defense, float x, float y) {
        this.tower_defense = tower_defense;
        position = new PVector(x, y);
        currentTime = 0;
        range = 50;
    }

    public void draw() {
        if (timer > currentTime) {
            tower_defense.image(image, position.x, position.y);
            currentTime++;
        } else {
            tower_defense.consumables.remove(this);
            tower_defense.claimedPoints.remove(position);
        }
    }

    public Consumable dupe() {
        return null;
    }

    protected boolean collidesWithEnemy(Enemy e) {
        return position.x - image.width / 2f < e.position.x - e.image.width / 2f + e.image.width
                && (position.x - image.width / 2f) + image.width > e.position.x - e.image.width / 2f
                && position.y - image.height / 2f < (e.position.y - e.image.height / 2f) + e.image.height
                && (position.y - image.height / 2f) + image.height > e.position.y - e.image.height / 2f;
    }

    public boolean allowedPositions() {
        return tower_defense.onClaimedPoint(position) && !tower_defense.positionOnTower(position);
    }
}
