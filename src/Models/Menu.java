package Models;

import processing.core.PConstants;
import processing.core.PVector;

import java.util.ArrayList;

public class Menu {
    Tower_Defense tower_defense;
    Weapon weapon;
    ArrayList<MenuItem> priorities;
    ArrayList<MenuItem> upgrades;
    ArrayList<MenuItem> sell;
    PVector position;

    Menu(Tower_Defense tower_defense, Weapon w) {
        this.weapon = w;
        this.tower_defense = tower_defense;
        priorities = new ArrayList<>();
        upgrades = new ArrayList<>();
        sell = new ArrayList<>();
        position = new PVector(tower_defense.width - 100, tower_defense.height + 62.5f);

        priorities.add(new MenuItem(tower_defense, "nearest", 4));
        priorities.add(new MenuItem(tower_defense, "lowest", 4));
        priorities.add(new MenuItem(tower_defense, "highest", 4));
        priorities.add(new MenuItem(tower_defense, "first", 4));
        upgrades.add(new MenuItem(tower_defense, "rate of fire", 3, w.upgradeCost));
        upgrades.add(new MenuItem(tower_defense, "radius", 3, w.upgradeCost));
        upgrades.add(new MenuItem(tower_defense, "damage", 3, w.upgradeCost));
        sell.add(new MenuItem(tower_defense, "sell", 1));
    }

    public void draw() {
        tower_defense.fill(0);
        tower_defense.rectMode(PConstants.CENTER);
        tower_defense.rect(position.x, position.y, 200, 125);
        drawItems();
        tower_defense.rectMode(PConstants.CORNER);
    }

    private void drawItems() {
        for (int i = 0; i < priorities.size(); i++) {
            priorities.get(i).draw(position.x - 97 + (200f / priorities.size()) * i + (200f / priorities.size() - 5) / 2, position.y - 147, i == weapon.priority);
        }
        for (int i = 0; i < upgrades.size(); i++) {
            upgrades.get(i).draw(position.x - 97 + (200f / upgrades.size()) * i + (200f / upgrades.size() - 5) / 2, position.y - 112, false);
        }
        for (int i = 0; i < sell.size(); i++) {
            sell.get(i).draw(position.x - 97 + (200f / sell.size()) * i + (200f / sell.size() - 5) / 2, position.y - 77, false);
        }
    }
}
