package Models;

import Enemies.StandardGoblin;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

import java.util.ArrayList;

public abstract class Weapon {
    protected final Tower_Defense tower_defense;
    public PImage weapon;
    public PImage base;
    public PImage projectileImg;
    public float damage;
    public float rateOfFire;
    public float timeToShoot;
    public int shootingRadius;
    public PVector position;
    public float angle;
    public int projectileSpeed;
    public int priority;
    public boolean menuOpen;
    public ArrayList<Projectile> projectiles;
    public Menu menu;
    public int cost;
    public int upgradeCost;

    public String name;
    public String description;

    protected Weapon(Tower_Defense tower_defense, float x, float y) {
        this.tower_defense = tower_defense;
        position = new PVector(x, y);
        this.projectiles = new ArrayList<>();
        priority = 0;
        upgradeCost = 100;
        menu = new Menu(tower_defense, this);
    }

    public void draw() {
        tower_defense.imageMode(PConstants.CENTER);
        tower_defense.image(base, position.x, position.y);
        drawWeapon();
        handleProjectiles();
        tower_defense.imageMode(PConstants.CORNER);
        if (timeToShoot <= 0 && findNearestEnemy() != null) {
            shoot();
            timeToShoot = rateOfFire;
        } else {
            timeToShoot--;
        }
    }

    public void drawMenu() {
        if (menuOpen) {
            tower_defense.noStroke();
            tower_defense.fill(tower_defense.yellow, 128);
            tower_defense.circle(position.x, position.y, shootingRadius);
            menu.draw();
        }
    }

    public void upgrade(MenuItem m) {
        if (tower_defense.coins >= upgradeCost) {
            tower_defense.coins -= upgradeCost;
            upgradeCost = (int) (upgradeCost * 1.2f);
            for (MenuItem item : menu.upgrades) {
                item.cost = upgradeCost;
            }
            switch (m.text) {
                case "rate of fire" -> rateOfFire = PApplet.constrain(rateOfFire * 0.9f, 1, 100);
                case "radius" -> shootingRadius += 10;
                case "damage" -> damage *= 1.1;
            }

        }
    }

    protected void handleProjectiles() {
        for (int i = 0; i < projectiles.size(); i++) {
            Projectile p = projectiles.get(i);
            if (p.position.x < 0 || p.position.x > tower_defense.width || p.position.y < 0 || p.position.y > tower_defense.height) {
                projectiles.remove(p);
            } else {
                p.draw();
            }
        }
    }

    public void onClickListener() {
        if (tower_defense.mouseX > position.x - base.width / 2f && tower_defense.mouseX < position.x + base.width / 2f && tower_defense.mouseY > position.y - base.height / 2f && tower_defense.mouseY < position.y + base.height / 2f) {
            tower_defense.rect(tower_defense.width, tower_defense.height, tower_defense.width, tower_defense.height);
            if (tower_defense.currentSelectedWeapon == null) {
                menuOpen = !menuOpen;
            }
        }
    }

    public Weapon dupe() {
        return null;
    }

    protected void shoot() {
        projectiles.add(new Projectile(tower_defense, this));
    }

    protected void drawWeapon() {
        Enemy e = null;
        switch (priority) {
            case 0 -> e = findNearestEnemy();
            case 1 -> e = findLowEnemy();
            case 2 -> e = findHighEnemy();
            case 3 -> e = findFirstEnemy();
        }
        if (e != null) {
            angle = tower_defense.angleBetween(e.position, position);
        }
        tower_defense.pushMatrix();
        tower_defense.translate(position.x, position.y);
        tower_defense.rotate(angle + Tower_Defense.radians(90));
        tower_defense.translate(-position.x, -position.y);
        tower_defense.image(weapon, position.x, position.y);
        tower_defense.popMatrix();
    }

    protected Enemy findNearestEnemy() {
        if (tower_defense.spawnedEnemies.size() > 0) {
            Enemy nearest = new StandardGoblin(tower_defense);
            for (Enemy e : tower_defense.spawnedEnemies) {
                float nearestDist = PVector.dist(position, nearest.position);
                float currentDist = PVector.dist(position, e.position);
                if (currentDist < nearestDist) {
                    nearest = e;
                }
            }
            if (PVector.dist(nearest.position, position) <= shootingRadius / 2f) {
                return nearest;
            } else {
                return null;
            }
        }
        return null;
    }

    Enemy findFirstEnemy() {
        Enemy target = new StandardGoblin(tower_defense);
        for (Enemy e : tower_defense.spawnedEnemies) {
            if (PVector.dist(e.position, position) < shootingRadius / 2f) { //enemy should be in radius to be targeting candidate
                if (e.current > target.current) {
                    target = e;
                } else if (e.current == target.current) {
                    float targetDist = PVector.dist(target.position, tower_defense.path.coors.get(target.current));
                    float eDist = PVector.dist(e.position, tower_defense.path.coors.get(e.current));
                    if (eDist < targetDist) { //if e is closer to its next checkpoint than target, it becomes the new target
                        target = e;
                    }
                }
            }
        }
        if (target.speed > 0) { //if speed is 0 target is still dummy made at start, do not return that
            return target;
        } else {
            return null;
        }
    }

    protected Enemy findLowEnemy() {
        if (tower_defense.spawnedEnemies.size() > 0) {
            Enemy lowest = new StandardGoblin(tower_defense);
            lowest.hp = Integer.MAX_VALUE;
            for (Enemy e : tower_defense.spawnedEnemies) {
                float dist = PVector.dist(position, e.position);
                if (dist < shootingRadius / 2f && e.hp < lowest.hp) {
                    lowest = e;
                }
            }
            if (PVector.dist(lowest.position, position) <= shootingRadius / 2f) {
                return lowest;
            } else {
                return null;
            }
        }
        return null;
    }

    protected Enemy findHighEnemy() {
        if (tower_defense.spawnedEnemies.size() > 0) {
            Enemy highest = new StandardGoblin(tower_defense);
            highest.hp = 0;
            for (Enemy e : tower_defense.spawnedEnemies) {
                float dist = PVector.dist(position, e.position);
                if (dist < shootingRadius / 2f && e.hp > highest.hp) {
                    highest = e;
                }
            }
            if (PVector.dist(highest.position, position) <= shootingRadius / 2f) {
                return highest;
            } else {
                return null;
            }
        }
        return null;
    }

    public void onHit(Enemy e, Projectile p) {
        e.hp -= damage;
        if (e.hp <= 0) {
            e.onDeath();
        }
        projectiles.remove(p);
    }
}
