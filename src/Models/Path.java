package Models;

import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

import java.util.ArrayList;

public class Path {

    private final Tower_Defense tower_defense;
    public ArrayList<PVector> coors;
    public final ArrayList<PVector> path;
    private final ArrayList<PImage> images;

    public void draw() {
        tower_defense.imageMode(PConstants.CENTER);
        if (coors.size() > 1) {
            for (int i = 0; i < path.size() && i < images.size(); i++) {
                PImage image = images.get(i);
                tower_defense.image(image, path.get(i).x, path.get(i).y);
            }
        }
    }

    Path(Tower_Defense tower_defense) {
        this.tower_defense = tower_defense;
        coors = new ArrayList<>();
        path = new ArrayList<>();
        images = new ArrayList<>();
        generate();
        setPath();
        setImages();
    }


    public void generate() {
        int random = (int) tower_defense.random(0, 5);
        switch (random) {
            case 0 -> map1();
            case 1 -> map2();
            case 2 -> map3();
            case 3 -> map4();
            case 4 -> map5();
        }
    }

    public void map1() {
        coors.add(new PVector(-50, 100));
        coors.add(new PVector(300, 100));
        coors.add(new PVector(300, 500));
        coors.add(new PVector(450, 500));
        coors.add(new PVector(450, 250));
        coors.add(new PVector(800, 250));
        coors.add(new PVector(800, 700));
        coors.add(new PVector(150, 700));
    }

    public void map2() {
        coors.add(new PVector(500, -50));
        coors.add(new PVector(500, 200));
        coors.add(new PVector(700, 200));
        coors.add(new PVector(700, 350));
        coors.add(new PVector(150, 350));
        coors.add(new PVector(150, 450));
        coors.add(new PVector(300, 450));
        coors.add(new PVector(300, 600));
        coors.add(new PVector(750, 600));
        coors.add(new PVector(750, 750));
        coors.add(new PVector(300, 750));
    }

    public void map3() {
        coors.add(new PVector(1050, 100));
        coors.add(new PVector(100, 100));
        coors.add(new PVector(100, 400));
        coors.add(new PVector(350, 400));
        coors.add(new PVector(350, 250));
        coors.add(new PVector(500, 250));
        coors.add(new PVector(500, 550));
        coors.add(new PVector(650, 550));
        coors.add(new PVector(650, 700));
        coors.add(new PVector(450, 700));
    }

    public void map4() {
        coors.add(new PVector(1050, 700));
        coors.add(new PVector(750, 700));
        coors.add(new PVector(750, 500));
        coors.add(new PVector(550, 500));
        coors.add(new PVector(550, 600));
        coors.add(new PVector(350, 600));
        coors.add(new PVector(350, 300));
        coors.add(new PVector(150, 300));
        coors.add(new PVector(150, 150));
        coors.add(new PVector(700, 150));
        coors.add(new PVector(700, 400));
        coors.add(new PVector(900, 400));
    }

    public void map5() {
        coors.add(new PVector(-50, 700));
        coors.add(new PVector(450, 700));
        coors.add(new PVector(450, 600));
        coors.add(new PVector(550, 600));
        coors.add(new PVector(550, 750));
        coors.add(new PVector(750, 750));
        coors.add(new PVector(750, 400));
        coors.add(new PVector(550, 400));
        coors.add(new PVector(550, 250));
        coors.add(new PVector(150, 250));
        coors.add(new PVector(150, 550));
        coors.add(new PVector(50, 550));
        coors.add(new PVector(50, 500));
    }

    public void setPath() {
        PVector prev = null;
        for (PVector p : coors) {
            if (prev != null) {
                tower_defense.claimPointsBetween(prev, p, path);
            }
            prev = p;
        }
        path.add(coors.get(coors.size() - 1));
    }

    public void setImages() {
        for (int i = 0; i < path.size() - 1; i++) {
            if (coors.contains(path.get(i))) {
                images.add(getCornerImage(coors.indexOf(path.get(i))));
            } else {
                if ((int) path.get(i + 1).y == (int) path.get(i).y) {
                    images.add(getRandomImageH());
                } else {
                    images.add(getRandomImageV());
                }
            }
        }
    }

    public PImage getRandomImageH() {
        int random = (int) tower_defense.random(0, 3);
        switch (random) {
            case 0 -> {
                return tower_defense.utils.path_1h;
            }
            case 1 -> {
                return tower_defense.utils.path_2h;
            }
        }
        return tower_defense.utils.path_3h;
    }

    public PImage getRandomImageV() {
        int random = (int) tower_defense.random(0, 3);
        switch (random) {
            case 0 -> {
                return tower_defense.utils.path_1v;
            }
            case 1 -> {
                return tower_defense.utils.path_2v;
            }
        }
        return tower_defense.utils.path_3v;
    }

    public PImage getCornerImage(int index) {
        if (index >= 1 && index < coors.size() - 1) {
            PVector prev = coors.get(index - 1);
            PVector current = coors.get(index);
            PVector next = coors.get(index + 1);
            if (prev.y < next.y && prev.x < next.x && current.y == prev.y) { //linksboven naar rechtsonder via rechtsboven
                return tower_defense.utils.path_corner_180;
            } else if (prev.y < next.y && prev.x < next.x && current.y > prev.y) { //linksboven naar rechtsonder via linksonder
                return tower_defense.utils.path_corner;
            } else if (prev.y < next.y && prev.x > next.x && current.y == prev.y) { //rechtsboven naar linksonder via linksboven
                return tower_defense.utils.path_corner_90;
            } else if (prev.y < next.y && prev.x > next.x && current.y > prev.y) { //rechtsboven naar linksonder via rechtsonder
                return tower_defense.utils.path_corner_270;
            } else if (prev.y > next.y && prev.x > next.x && current.y < prev.y) { //rechtsonder naar linksboven via rechtsboven
                return tower_defense.utils.path_corner_180;
            } else if (prev.y > next.y && prev.x > next.x && current.y == prev.y) { //rechtsonder naar linksboven via linksonder
                return tower_defense.utils.path_corner;
            } else if (prev.y > next.y && prev.x < next.x && current.y < prev.y) { //linksonder naar rechtsboven via linksboven
                return tower_defense.utils.path_corner_90;
            } else if (prev.y > next.y && prev.x < next.x && current.y == prev.y) { //linksonder naar rechtsboven via rechtssonder
                return tower_defense.utils.path_corner_270;
            }
        }
        return getRandomImageH();
    }
}
