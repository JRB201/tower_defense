package Models;

import Consumables.*;
import Weapons.*;
import processing.core.PConstants;

import java.util.ArrayList;

public class Shop {
    Tower_Defense tower_defense;
    ArrayList<ShopItem> favorites;
    ArrayList<ShopItem> weapons;
    ArrayList<ShopItem> consumables;
    ArrayList<ShopItem> improvements;
    boolean isOpen = false;

    public Shop(Tower_Defense tower_defense) {
        this.tower_defense = tower_defense;
        favorites = new ArrayList<>();
        weapons = new ArrayList<>();
        consumables = new ArrayList<>();
        improvements = new ArrayList<>();
        addDefaultToFavorites();
    }

    public void draw() {
        tower_defense.fill(tower_defense.brown);
        tower_defense.rectMode(PConstants.CORNER);
        tower_defense.rect(0, tower_defense.height - 100, tower_defense.width, 100);
        tower_defense.fill(0);
        for (ShopItem i : favorites) {
            i.draw();
        }
        if (isOpen) {
            tower_defense.fill(tower_defense.brown);
            tower_defense.rect(98, 98, tower_defense.width - 198, tower_defense.height - 323);
            for (ShopItem i : weapons) {
                i.draw();
            }
            for (ShopItem i : consumables) {
                i.draw();
            }
            for (ShopItem i : improvements) {
                i.draw();
            }
            tower_defense.fill(0);
            tower_defense.textSize(60);
            tower_defense.textAlign(PConstants.CENTER);
            tower_defense.text("Weapons", tower_defense.width / 2f, tower_defense.height / 5f * 2.8f);
            tower_defense.text("Consumables", tower_defense.width / 2f, tower_defense.height / 5f * 1.8f);
            tower_defense.text("Improvements", tower_defense.width / 2f, tower_defense.height / 5f * 0.8f);

        }
        tower_defense.imageMode(PConstants.CENTER);
        tower_defense.image(isOpen ? tower_defense.utils.downButton : tower_defense.utils.upButton, tower_defense.width / 2f, tower_defense.height - 100);
        tower_defense.imageMode(PConstants.CORNER);
        tower_defense.image(tower_defense.utils.coin, 20, tower_defense.height - 95);
        tower_defense.textSize(25);
        tower_defense.textAlign(PConstants.CORNER);
        tower_defense.text(tower_defense.coins, 25 + tower_defense.utils.coin.width, tower_defense.height - 95 + tower_defense.utils.coin.height / 1.5f);
        tower_defense.image(tower_defense.utils.wave, 20, tower_defense.height - 50);
        tower_defense.text(tower_defense.currentWaveNumber, 25 + tower_defense.utils.wave.width, tower_defense.height - 50 + tower_defense.utils.wave.height / 1.5f);
    }

    public void positionShopItems() {
        positionArrayList(favorites, tower_defense.height - 50);
        positionArrayList(weapons, tower_defense.height / 5 * 3);
        positionArrayList(consumables, tower_defense.height / 5 * 2);
        positionArrayList(improvements, tower_defense.height / 5);
    }

    private void positionArrayList(ArrayList<ShopItem> arrayList, int height) {
        int distance = 50;
        int total;
        total = arrayList.size() * distance;
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList.get(i).position.x = (tower_defense.width / 2f) - (total / 2f) + ((float) (total / (arrayList.size() - 1)) * i);
            arrayList.get(i).position.y = height;
        }
    }

    public void showDescriptions() {
        for (ShopItem s : favorites) {
            if (s.mouseOverShopItem()) {
                s.displayDescription();
            }
        }
        if (isOpen) {
            for (ShopItem s : weapons) {
                if (s.mouseOverShopItem()) {
                    s.displayDescription();
                }
            }
            for (ShopItem s : consumables) {
                if (s.mouseOverShopItem()) {
                    s.displayDescription();
                }
            }
            for (ShopItem s : improvements) {
                if (s.mouseOverShopItem()) {
                    s.displayDescription();
                }
            }
        }
    }

    public void addDefaultToFavorites() {
        favorites.add(new ShopItem(tower_defense, 0, 0, new Ballista(tower_defense, 0, 0)));
        favorites.add(new ShopItem(tower_defense, 0, 0, new Cannon(tower_defense, 0, 0)));
        favorites.add(new ShopItem(tower_defense, 0, 0, new Bomb(tower_defense, 0, 0)));
        favorites.add(new ShopItem(tower_defense, 0, 0, new Spikes(tower_defense, 0, 0)));
        favorites.add(new ShopItem(tower_defense, 0, 0, new Medkit(tower_defense, 0, 0)));
        for (int i = favorites.size(); i < 9; i++) {
            favorites.add(new ShopItem(tower_defense, 0, 0));
        }
    }
}
