package StatusEffects;

import Models.Enemy;
import Models.Tower_Defense;
import Models.StatusEffect;
import Particles.Fire;

public class OnFire extends StatusEffect {
    public OnFire(Tower_Defense tower_defense, Enemy e, int duration) {
        super(tower_defense, e, duration);
        this.filter = tower_defense.color(255, 60, 60);
    }

    @Override
    public boolean equals(StatusEffect e) {
        return e instanceof OnFire ;
    }

    @Override
    public void onApply() {
        this.enemy.speed *= 1.25;
    }

    @Override
    public void whileApplied(){
        if(this.timeLeft % 5 == 0){
            this.enemy.hp--;
            particles.add(new Fire(tower_defense,this, enemy.position,"rectangle", 50));
        }
        this.timeLeft--;
        if(this.enemy.hp <= 0) {
            this.enemy.onDeath();
        }
        for (int i = 0; i < particles.size() ; i++) {
            particles.get(i).draw();
            if(particles.get(i).duration <= 0){
                particles.remove(particles.get(i));
            }
        }
    }

    @Override
    public void onExpire() {
        this.enemy.speed /= 1.25;
    }
}
