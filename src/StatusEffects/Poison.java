package StatusEffects;

import Models.Enemy;
import Models.Tower_Defense;
import Models.StatusEffect;

public class Poison extends StatusEffect {
    public Poison(Tower_Defense tower_defense, Enemy e, int duration) {
        super(tower_defense, e, duration);
        this.filter = tower_defense.color(0, 230, 0);
    }

    @Override
    public boolean equals(StatusEffect e) {
        return e instanceof Poison ;
    }

    @Override
    public void whileApplied(){
        if(this.timeLeft % 5 == 0){
            this.enemy.hp--;
        }
        this.timeLeft--;
        if(this.enemy.hp <= 0) {
            this.enemy.onDeath();
        }
    }
}
