package StatusEffects;

import Models.Enemy;
import Models.Tower_Defense;
import Models.StatusEffect;

public class Slow extends StatusEffect {
    public Slow(Tower_Defense tower_defense, Enemy e, int duration) {
        super(tower_defense, e, duration);
        this.filter = tower_defense.color(60, 60, 255);
    }

    @Override
    public boolean equals(StatusEffect e) {
        return e instanceof Slow;
    }

    @Override
    public void onApply() {
        this.enemy.speed *= 0.5;
    }

    @Override
    public void whileApplied(){
        this.timeLeft--;
    }

    @Override
    public void onExpire() {
        this.enemy.speed *= 2;
    }
}
