package Consumables;

import Models.Consumable;
import Models.Tower_Defense;

public class CarpetBomb extends Consumable {
    int timer;
    int currentTimer;
    int path;
    int currentPath;

    public CarpetBomb(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        image = tower_defense.utils.bomb;
        name = "Carpetbomb";
        description = "There is a reason why this is so expensive.";
        path = tower_defense.path.path.size();
        currentPath = path - 2;
        timer = 10;
        currentTimer = timer;
        cost = 10000;
        damage = 100;
    }

    public void draw() {
        if (currentTimer == 0 && currentPath >= 0) {
            tower_defense.consumables.add(new Bomb(tower_defense, tower_defense.path.path.get(currentPath).x, tower_defense.path.path.get(currentPath).y));
            currentPath--;
            currentTimer = timer;
        } else {
            currentTimer--;
            if (currentPath <= 0) {
                tower_defense.consumables.remove(this);
            }
        }
    }

    @Override
    public CarpetBomb dupe() {
        return new CarpetBomb(tower_defense, position.x, position.y);
    }
}
