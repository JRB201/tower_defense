package Consumables;

import Models.Consumable;
import Models.Drawable;
import Models.Enemy;
import Models.Tower_Defense;
import processing.core.PConstants;
import processing.core.PVector;

public class Bomb extends Consumable {

    int flashtimer;

    public Bomb(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        damage = 100;
        timer = 100;
        image = tower_defense.utils.bomb;
        cost = 100;
        flashtimer = timer/2;
        name = "Bomb";
        description = "This is da Bomb";
    }

    @Override
    public void draw() {
        if (timer > currentTime) {
            if(currentTime == flashtimer){
                tower_defense.tint(225,0,0);
                flashtimer += (timer-currentTime)/2;
            }
            tower_defense.imageMode(PConstants.CENTER);
            tower_defense.image(image, position.x, position.y);
            tower_defense.noTint();

            currentTime++;
        } else {
            tower_defense.drawables.add(new Drawable(tower_defense, position, tower_defense.utils.explosion, 10));
            tower_defense.consumables.remove(this);
            for (Enemy e : tower_defense.spawnedEnemies) {
                if (PVector.dist(e.position, position) < range) {
                    e.hp -= damage * (1-(PVector.dist(e.position, e.position) / 100));
                }
            }
        }
    }

    @Override
    public Bomb dupe() {
        return new Bomb(tower_defense, position.x, position.y);
    }
}
