package Consumables;

import Models.Consumable;
import Models.Enemy;
import Models.Tower_Defense;
import processing.core.PConstants;

public class Spikes extends Consumable {
    int uses;
    int currentUses;
    int dmgCooldown;
    int currentDmgCooldown;

    public Spikes(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        damage = 25;
        cost = 250;
        uses = 5;
        currentUses = 0;
        dmgCooldown = (int) (tower_defense.frameRate);
        currentDmgCooldown = dmgCooldown;
        image = tower_defense.utils.spikes;
        name = "Spikes";
        description = "Stabby stabby";
    }

    @Override
    public void draw() {
        if (uses > currentUses) {
            tower_defense.imageMode(PConstants.CENTER);
            tower_defense.image(image, position.x, position.y);
        } else {
            tower_defense.consumables.remove(this);
        }
        if (currentDmgCooldown <= 0) {
            for (Enemy e : tower_defense.spawnedEnemies) {
                if (collidesWithEnemy(e)) {
                    e.hp -= damage;
                    currentUses++;
                    currentDmgCooldown = dmgCooldown;
                }
            }
        } else {
            currentDmgCooldown--;
        }
    }


    public Spikes dupe() {
        return new Spikes(tower_defense, position.x, position.y);
    }
}
