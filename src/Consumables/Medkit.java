package Consumables;

import Models.Consumable;
import Models.Drawable;
import Models.Tower_Defense;

public class Medkit extends Consumable {

    public Medkit(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        cost = 1000;
        name = "Medkit";
        description = "A medkit for a tower, how does this work?";
        image = tower_defense.utils.medkit;

    }

    public void draw() {
        if(tower_defense.tower.hp < tower_defense.tower.maxHp){
            tower_defense.tower.hp += 50;
            tower_defense.drawables.add(new Drawable(tower_defense, position, tower_defense.utils.healing, 10));
        }else{
            tower_defense.coins += cost;
        }
        tower_defense.consumables.remove(this);
    }

    public Medkit dupe() {
        return new Medkit(tower_defense, position.x, position.y);
    }

    @Override
    public boolean allowedPositions() {
        return tower_defense.onClaimedPoint(position) && tower_defense.positionOnTower(position);
    }
}
