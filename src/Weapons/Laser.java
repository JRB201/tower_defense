package Weapons;

import Models.Tower_Defense;
import Models.Weapon;

public class Laser extends Weapon {
    public Laser(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        this.damage = 5;
        this.rateOfFire = 4;
        this.timeToShoot = rateOfFire;
        this.shootingRadius = 500;
        this.projectileSpeed = 10;
        this.weapon = tower_defense.utils.laser;
        this.base = tower_defense.utils.laser_base;
        this.projectileImg = tower_defense.utils.laserbolt;
        this.cost = 5000;
        name = "Laser";
        description = "PEW! PEW! PEW! PEW! PEW! PEW! PEW!";
    }

    @Override
    public Laser dupe() {
        return new Laser(tower_defense, position.x, position.y);
    }
}
