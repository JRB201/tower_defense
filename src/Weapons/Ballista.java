package Weapons;

import Models.*;
import StatusEffects.Poison;

public class Ballista extends Weapon {

    public boolean nextToBrewery = false;

    public Ballista(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        this.damage = 25;
        this.rateOfFire = 30;
        this.timeToShoot = rateOfFire;
        this.shootingRadius = 300;
        this.projectileSpeed = 5;
        this.weapon = tower_defense.utils.ballista;
        this.base = tower_defense.utils.base;
        this.projectileImg = tower_defense.utils.bolt;
        this.cost = 1000;
        base.resize(32, 32);
        weapon.resize(32, 32);
        name = "Ballista";
        description = "This is the most basic of towers, it shoots arrows.";
    }

    @Override
    public void onHit(Enemy e, Projectile p) {
        super.onHit(e, p);
        if (nextToBrewery) {
            e.addStatusEffect(new Poison(tower_defense, e, 4));
        }
    }

    @Override
    public Ballista dupe() {
        return new Ballista(tower_defense, position.x, position.y);
    }
}
