package Weapons;

import Models.*;
import processing.core.PVector;


public class Cannon extends Weapon {
    public Cannon(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        this.damage = 80;
        this.rateOfFire = 100;
        this.timeToShoot = rateOfFire;
        this.shootingRadius = 200;
        this.projectileSpeed = 5;
        this.weapon = tower_defense.utils.cannon;
        this.base = tower_defense.utils.base;
        this.projectileImg = tower_defense.utils.cannonball;
        this.cost = 3000;
        base.resize(32, 32);
        weapon.resize(32, 32);
        name = "Cannon";
        description = "This is a Cannon. Cannon go boom.";
    }

    @Override
    public Cannon dupe() {
        return new Cannon(tower_defense, position.x, position.y);
    }

    @Override
    public void onHit(Enemy e, Projectile p) {
        super.onHit(e, p);
        tower_defense.drawables.add(new Drawable(tower_defense, e.position,tower_defense.utils.explosion, 10));
        for (int i = 0; i < tower_defense.spawnedEnemies.size(); i++) {
            Enemy enemy = tower_defense.spawnedEnemies.get(i);
            if (PVector.dist(e.position, enemy.position) < 100 && e != enemy) {
                enemy.hp -= damage * (1-(PVector.dist(e.position, enemy.position) / 100));
                if (enemy.hp <= 0) {
                    tower_defense.spawnedEnemies.remove(enemy);
                }
            }
        }

    }
}
