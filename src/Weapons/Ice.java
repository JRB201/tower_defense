package Weapons;

import Models.*;
import StatusEffects.Slow;

public class Ice extends Weapon {
    public Ice(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        this.damage = 0;
        this.rateOfFire = 200;
        this.timeToShoot = rateOfFire;
        this.shootingRadius = 500;
        this.projectileSpeed = 10;
        this.weapon = tower_defense.utils.ice;
        this.base = tower_defense.utils.iceBase;
        this.projectileImg = tower_defense.utils.snowBall;
        this.cost = 9000;
        name = "SnowBall";
        description = "I think you've met FireBall, this is his brother, it slows enemies.";
    }

    @Override
    public void onHit(Enemy e, Projectile p) {
        super.onHit(e, p);
        e.addStatusEffect(new Slow(tower_defense, e, 5));
    }

    @Override
    public Ice dupe() {
        return new Ice(tower_defense, position.x, position.y);
    }
}
