package Weapons;

import Models.*;
import StatusEffects.OnFire;

public class Fire extends Weapon {
    public Fire(Tower_Defense tower_defense, float x, float y) {
        super(tower_defense, x, y);
        this.damage = 10;
        this.rateOfFire = 150;
        this.timeToShoot = rateOfFire;
        this.shootingRadius = 400;
        this.projectileSpeed = 5;
        this.weapon = tower_defense.utils.fire;
        this.base = tower_defense.utils.fireBase;
        this.projectileImg = tower_defense.utils.fireBall;
        this.cost = 7000;
        name = "FireBall";
        description = "This tower shoots fireballs. As you might expect, it sets enemies ablaze.";
    }

    @Override
    public void onHit(Enemy e, Projectile p) {
        super.onHit(e, p);
        e.addStatusEffect(new OnFire(tower_defense, e, 4));
    }

    @Override
    public Fire dupe() {
        return new Fire(tower_defense, position.x, position.y);
    }
}
