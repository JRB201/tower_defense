package Particles;

import Models.Particle;
import Models.StatusEffect;
import Models.Tower_Defense;
import processing.core.PVector;

public class Fire extends Particle {
    int height;

    public Fire(Tower_Defense tower_defense, StatusEffect statusEffect, PVector location, String shape, int duration) {
        super(tower_defense, statusEffect, location, shape, duration);
        this.location.y = location.y - statusEffect.enemy.image.height / 2f;
        this.location.x = (int) tower_defense.random(location.x - statusEffect.enemy.image.width / 2f, location.x + statusEffect.enemy.image.width / 2f);
        height = 0;
    }

    @Override
    public void draw() {
        location.y = statusEffect.enemy.position.y;
        tower_defense.fill(statusEffect.filter);
        switch (shape) {
            case "rectangle" -> {
                tower_defense.rect(this.location.x, this.location.y-height, duration / 3f, duration / 3f);
            }
            case "circle" -> {
                tower_defense.circle(this.location.x, this.location.y, duration / 3f);
            }
        }
        duration--;
        height++;
    }
}
